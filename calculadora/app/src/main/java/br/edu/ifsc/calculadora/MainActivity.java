package br.edu.ifsc.calculadora;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ImageButton mais =(ImageButton) findViewById(R.id.mais);
        ImageButton menos =(ImageButton) findViewById(R.id.menos);
        ImageButton vezes =(ImageButton) findViewById(R.id.vezes);
        mais.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v) {
                Intent it = new Intent(MainActivity.this, Mais.class);
                startActivity(it);
            }
        });
        menos.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v) {
                Intent it = new Intent(MainActivity.this, Menos.class);
                startActivity(it);
            }
        });
        vezes.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v) {
                Intent it = new Intent(MainActivity.this, vezes.class);
                startActivity(it);
            }
        });
    }
}
