package br.edu.ifsc.calculadora;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

public class vezes extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vezes);
        ImageButton calc =(ImageButton) findViewById(R.id.igual);
        calc.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v) {
                EditText n1 = findViewById(R.id.n1);
                TextView result = findViewById(R.id.resultado);
                EditText n2 = findViewById(R.id.n2);
                String nu1 = n1.getText().toString();
                String nu2 = n2.getText().toString();
                float res = Float.parseFloat(nu1)* Float.parseFloat(nu2);
                String RF = "RESULTADO: " + Float.toString(res);
                result.setText(RF);
            }
        });
    }
}
